import './GiphyAttribution.css';

export const GiphyAttribution = () => (
  <img
    src="/giphy-attribution.png"
    alt="Powered By GIPHY"
    width="185"
    height="22"
    className="giphy-attribution"
  />
);
